
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[2.783418,-2.936875],[2.698748,-3.603625],[-2.444751,-3.603625],[-2.783418,-1.719791],[-2.444751,-1.719791],[-1.877220,-2.561166],[-0.762001,-2.735791],[2.010832,-2.735791],[-0.313533,0.358511],[-1.534585,3.603625],[-0.169335,3.603625],[0.199759,1.583531],[1.076356,-0.508496],[2.783415,-2.936875],[2.783418,-2.936875]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
