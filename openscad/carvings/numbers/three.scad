
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[2.836334,1.529296],[2.569435,0.585395],[1.947333,0.018525],[0.709084,-0.396871],[1.675639,-0.755546],[2.202656,-1.210464],[2.465917,-2.016121],[2.276078,-2.705360],[1.748896,-3.251725],[-0.063500,-3.741204],[-1.145480,-3.565587],[-1.885156,-3.130017],[-2.444749,-2.026704],[-2.268802,-1.541194],[-1.830916,-1.349371],[-1.420812,-1.500183],[-1.248833,-1.857371],[-1.428749,-2.481787],[-1.047750,-3.036089],[-0.126999,-3.264954],[0.927365,-2.905121],[1.322917,-1.942037],[1.150276,-1.256601],[0.719666,-0.832110],[-0.391583,-0.555621],[-0.920749,-0.576791],[-0.920749,-0.100542],[-0.179917,-0.153462],[1.027906,0.186527],[1.467611,0.708252],[1.640417,1.550454],[1.160198,2.812517],[-0.169333,3.264954],[-1.407583,2.917027],[-1.693333,2.450038],[-1.598084,1.973788],[-1.763448,1.550454],[-2.190749,1.381121],[-2.648479,1.595433],[-2.836333,2.111371],[-2.676261,2.665177],[-2.190750,3.192194],[-1.371864,3.586257],[-0.211666,3.741204],[1.014677,3.577162],[1.979084,3.119433],[2.610115,2.419611],[2.836334,1.529288],[2.836334,1.529296]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
