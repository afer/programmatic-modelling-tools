
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[2.883955,1.328208],[2.692297,0.397371],[2.160320,-0.339990],[1.352514,-0.825335],[0.333372,-1.000125],[-0.829471,-0.742156],[-1.730378,0.079375],[-1.645708,-1.031875],[-1.013354,-2.746375],[-0.452106,-3.175000],[0.269875,-3.317875],[1.254125,-2.989791],[1.359958,-2.693458],[1.542521,-2.088885],[2.026708,-1.857375],[2.420937,-2.005542],[2.577042,-2.375958],[2.391172,-2.892888],[1.891771,-3.328458],[0.301625,-3.741208],[-0.740833,-3.554015],[-1.775354,-2.910416],[-2.567781,-1.687380],[-2.883958,0.238125],[-2.591097,1.986525],[-1.858698,3.057260],[-0.906033,3.594199],[0.047625,3.741208],[1.164993,3.556165],[2.065073,3.046677],[2.665511,2.281204],[2.883958,1.328208],[2.883955,1.328208],[1.688039,1.381128],[1.238247,2.731826],[0.026455,3.233211],[-1.193274,2.756961],[-1.635128,1.455211],[-1.169461,0.066149],[-0.626073,-0.315843],[0.089955,-0.449788],[1.250154,0.048951],[1.688039,1.381128],[1.688039,1.381128]];
number_0_numbers = [[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32],
				[33,34,35,36,37,38,39,40,41,42,43]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
