
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.


// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);


number_0_points = [[2.772833,1.227666],[2.577868,0.255654],[2.030677,-0.500062],[1.187814,-0.989872],[0.105833,-1.164167],[-0.830792,-1.017323],[-1.703916,-0.497417],[-1.703916,-2.614083],[1.915583,-2.614083],[2.095500,-3.862917],[1.693333,-3.862917],[1.395677,-3.549385],[0.836083,-3.481917],[-2.360083,-3.481917],[-2.360083,0.317500],[-1.905000,0.423333],[-1.145646,-0.337343],[-0.243417,-0.550332],[0.514284,-0.414403],[1.087438,-0.026458],[1.576917,1.386417],[1.439499,2.212413],[1.049073,2.842948],[0.438382,3.245280],[-0.359833,3.386667],[-1.247510,3.197490],[-1.619250,2.730501],[-1.513416,2.222501],[-1.697301,1.807105],[-2.127250,1.629834],[-2.584979,1.829594],[-2.772833,2.307167],[-2.575057,2.920835],[-2.032000,3.414448],[-0.211667,3.862917],[1.000290,3.669936],[1.943364,3.128698],[2.555048,2.295756],[2.772833,1.227667],[2.772833,1.227666]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}

