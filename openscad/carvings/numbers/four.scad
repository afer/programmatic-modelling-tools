
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[3.042709,1.031874],[1.434042,1.031874],[1.434042,-3.603626],[0.428625,-3.603626],[-3.042708,1.201207],[-2.883958,1.635124],[0.428625,1.635124],[0.428625,2.079624],[0.336019,2.910415],[-0.248708,3.264957],[-0.597958,3.286127],[-0.597958,3.603627],[2.460625,3.603627],[2.460625,3.286127],[2.111375,3.264957],[1.542521,2.934226],[1.434042,2.079624],[1.434042,1.635124],[2.809875,1.635124],[3.042709,1.031874],[0.428624,1.031874],[-2.291291,1.031874],[0.428624,-2.704042],[0.428624,1.031874]];
number_0_numbers = [[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],
				[20,21,22,23]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
