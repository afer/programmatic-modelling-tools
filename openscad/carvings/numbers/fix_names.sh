# script to change the module name to the one using the correct number.

for fname in *.scad; do
    name=$(basename $fname .scad)
    sed -i 's/path[0-9]*/number/g' $fname
    sed -i 's/profile_scale = .*/profile_scale = 4;/' $fname
    sed -i 's/width = .*/width = 2;/' $fname
    sed -i 's/scale(-profile_scale, 1/ -profile_scale, width/' $fname
done
