
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[2.889250,0.047625],[2.678741,-1.495392],[2.086240,-2.692135],[1.170285,-3.466207],[-0.010583,-3.741208],[-1.264047,-3.410810],[-2.164292,-2.550583],[-2.707349,-1.356981],[-2.889250,-0.026458],[-2.679072,1.504322],[-2.088885,2.694781],[-1.179215,3.466537],[-0.010583,3.741208],[1.161355,3.469183],[2.078302,2.708010],[2.675764,1.540040],[2.889250,0.047625],[2.889250,0.047625],[1.746250,-0.026455],[1.398323,2.190753],[0.866676,2.944154],[0.010583,3.233211],[-0.558601,3.118613],[-1.129771,2.665680],[-1.569971,1.710700],[-1.746250,0.089961],[-1.420812,-2.000247],[-0.880070,-2.874695],[0.031750,-3.233205],[0.893465,-2.940510],[1.416844,-2.185455],[1.746250,-0.026455],[1.746250,-0.026455]];
number_0_numbers = [[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
				[18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]];


module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
