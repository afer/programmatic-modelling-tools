
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

height = 1;
width = 5;


number_0_points = [[1.804458,3.603625],[1.804458,3.286125],[1.465791,3.264955],[0.687917,3.020214],[0.513291,2.132539],[0.513291,-3.603628],[0.174624,-3.603628],[-1.793875,-3.275544],[-1.793875,-2.958044],[-1.370542,-2.936874],[-0.641614,-2.741083],[-0.492125,-1.942040],[-0.492125,2.111376],[-0.596636,2.886605],[-1.455208,3.264959],[-1.804458,3.286129],[-1.804458,3.603628],[1.804458,3.603625]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
