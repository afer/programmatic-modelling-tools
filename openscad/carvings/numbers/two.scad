
// Module names are of the form poly_<inkscape-number-id>().
// As a result you can associate a polygon in this OpenSCAD program with the
//  corresponding SVG element in the Inkscape document by looking for 
//  the XML element with the attribute id="inkscape-number-id".

// Paths have their own variables so they can be imported and used 
//  in polygon(points) structures in other programs.
// The NN_points is the list of all polygon XY vertices. 
// There may be an NN_numbers variable as well. If it exists then it 
//  defines the nested numbers. Both must be used in the 
//  polygon(points, numbers) variant of the command.

profile_scale = 4;

// helper functions to determine the X,Y dimensions of the profiles
function min_x(shape_points) = min([ for (x = shape_points) min(x[0])]);
function max_x(shape_points) = max([ for (x = shape_points) max(x[0])]);
function min_y(shape_points) = min([ for (x = shape_points) min(x[1])]);
function max_y(shape_points) = max([ for (x = shape_points) max(x[1])]);

number_0_points = [[2.825750,1.693333],[2.423583,1.587499],[2.317750,1.830916],[1.760802,2.627312],[0.751416,2.804583],[-1.651000,2.804583],[0.518583,1.301749],[1.908968,-0.205052],[2.370666,-1.608666],[2.178182,-2.440120],[1.640416,-3.092979],[0.816901,-3.519620],[-0.232833,-3.672416],[-1.216256,-3.513336],[-2.028031,-3.082395],[-2.579853,-2.449049],[-2.783417,-1.682749],[-2.574396,-1.047750],[-2.063750,-0.793750],[-1.643063,-0.968375],[-1.460500,-1.365250],[-1.587500,-1.703917],[-1.809750,-2.190750],[-1.353344,-2.868083],[-0.317500,-3.164417],[0.727604,-2.774156],[1.121833,-1.756834],[0.816239,-0.527844],[-0.148167,0.836083],[-2.825750,2.931583],[-2.825750,3.672416],[2.317750,3.672416],[2.825750,1.693333]];

module poly_number(h, profile_scale)  {
  scale([profile_scale, -profile_scale, 1])
  union()  {
    linear_extrude(height=h)
      polygon(number_0_points);
  }
}
