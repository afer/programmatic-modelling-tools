wall_width = 5;

hole_size = 25;
rack_height = 70;

ncols = 2;
nrows = 2;

// Start code

difference(){
  cube([hole_size * ncols + (ncols+1) * wall_width,
        hole_size * nrows + (nrows+1) * wall_width,
        rack_height]);

    union(){
    for (row = [0 : 1: nrows]){
        for (col=[0 : 1 : ncols]){
      translate([(wall_width + hole_size) * row + wall_width ,
                 (wall_width + hole_size) * col + wall_width,
                 wall_width])
        cube([hole_size, hole_size, rack_height]);
        }

    }
  }
}
