use <screwhole.scad>

inner_len = 55;
inner_height = 87;
inner_wid = 24;

eth_loc = 30;
eth_len = 18;

power_loc = 8;
power_len = 15;


hole_loc = 40;
screwhole_rad = 3; // Radius of holes for fixing to rails
distance_to_centre = 25;
distance_to_top = 13;
head_rad = 5;
head_depth = 3;

wall_wid = 5;

//-----

outer_len = inner_len + 2 * wall_wid;
outer_wid = inner_wid + 2 * wall_wid;
outer_height = inner_height + wall_wid;
difference(){
cube([outer_len, outer_wid, outer_height]);
union(){
translate([wall_wid, wall_wid, wall_wid]) cube([inner_len, inner_wid, inner_height]);
 translate([0, wall_wid, hole_loc]) cube([outer_len, outer_wid - wall_wid, outer_height-hole_loc]);
 translate([power_loc,wall_wid,0]) cube([power_len, inner_wid, wall_wid]);
 translate([eth_loc,wall_wid,0]) cube([eth_len, inner_wid, wall_wid]);
        /* Screw holes */
        for (x = [outer_len / 2 + distance_to_centre,   outer_len / 2 -  distance_to_centre]){
          translate([x , 0, outer_height - distance_to_top ]) rotate([-90, 0, 0])
            screwhole(depth = wall_wid, head_depth = head_depth, head_rad = head_rad, screw_rad = screwhole_rad);
        }
}
}
