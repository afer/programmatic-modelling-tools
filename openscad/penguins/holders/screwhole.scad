module screwhole(depth, head_depth, head_rad, screw_rad, fa=2, fs=0.4){
  union(){

    screw_depth = depth-head_depth;
     cylinder(r=screw_rad, h=screw_depth, $fa=fa, $fs=fs);
     translate([0, 0, screw_depth]) cylinder(r=head_rad, h=head_depth, $fa=fa, $fs=fs);
  }
}


