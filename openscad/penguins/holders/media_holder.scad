use <screwhole.scad>;
bottle_rad = 43;
bottle_height = 30;


wall_wid = 3;

nholes = 2;
hole_dist = 25;
hole_rad = 2;
head_rad = 4;
head_depth = 2;

fa=2;
fs=0.4;
///
outer_rad = bottle_rad + wall_wid;
outer_height = bottle_height + wall_wid;

start_hole = outer_rad - 42;

difference()
{
  cylinder(r=outer_rad,h=outer_height, $fa=fa, $fs=fs);
  union(){
    translate([0, 0, wall_wid]) cylinder(r=bottle_rad, h=bottle_height, $fa=fa, $fs=fs);
    for (x = [0 : nholes-1 : 1]){
      translate([start_hole - x * hole_dist, 0, 0]) screwhole(depth=wall_wid, head_depth=head_depth, head_rad=head_rad, screw_rad=hole_rad);
    }
  for (x = [0 : 90 : 90]){
     rotate([0,0,x]) translate([0, -outer_rad, wall_wid]) cube([3 * wall_wid, 2 * outer_rad, bottle_height]);
  }
  }
}
