/* include <penguin.scad>; */
use </home/alan/Documents/sync_docs/PhD/3D/openscad/carvings/logo/penguin.scad>;
use </home/alan/Documents/sync_docs/PhD/3D/openscad/carvings/numbers/three.scad>;
use <screwhole.scad>

module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );
}

//chibio measurements
chi_height = 120;
chi_wid = 55;
chi_len = 65;

wall_wid = 5;
back_hole_height = 50;
extra_distance_height = 15;

pin_height_loc = 70;
pin_height = 8;
pin_len_loc = 15;
pin_wid = 12;

//multiplying by four to be sure it'll fit
outer_wid = chi_wid + 4 * wall_wid;
outer_len = chi_len + 2 * wall_wid;
outer_height = chi_height + 2 * wall_wid;

//screwhole
screwhole_rad = 3; // Radius of holes for fixing to rails
head_rad = 5;
head_depth = 3;
distance_to_centre = 25;
distance_to_top = 13;

// Hole of pin connector
pin_hole_height = pin_height_loc + pin_height + extra_distance_height;

front_hole_fraction = 4 / 5; // Fraction of the max outer_wid or outer_len to use to remove the top part of side panels

// carving depth
carv_depth = 1;
number_frac = 0.8;

/* Start of code */
inner_len = outer_len - 2 * wall_wid;
inner_wid = outer_wid - wall_wid;
top_hole_height =  outer_height - back_hole_height;
front_hole_rad = outer_wid * front_hole_fraction;

difference()
    {
      cube([outer_len, outer_wid, outer_height]);

      union()
        {
        /* Hollowness */
        translate([wall_wid, 0, wall_wid]) cube([inner_len, inner_wid, back_hole_height - wall_wid + extra_distance_height]);
        translate([wall_wid, wall_wid, back_hole_height]) cube([inner_len, inner_wid, outer_height - back_hole_height]);



        // Make a cylindrical hole on the front top.
        translate([0, outer_wid, outer_height]) rotate([0, 90, 0]) cylinder(h=outer_wid, r = front_hole_rad, $fa=2, $fs=0.4);
        /* Screw holes */
        for (x = [outer_len / 2 + distance_to_centre,   outer_len / 2 -  distance_to_centre]){
          translate([x , 0, outer_height - distance_to_top ]) rotate([-90, 0, 0])
            screwhole(depth = wall_wid, head_depth = head_depth, head_rad = head_rad, screw_rad = screwhole_rad);
        }

        // Pin holes
        translate([chi_len - pin_len_loc, 0, pin_hole_height]) cube([pin_wid, wall_wid, pin_height + pin_height]);
        translate([chi_len - 30, 0, back_hole_height+15]) cube([20, wall_wid, 10]);

        // Penguin drawing
        translate([0, outer_len/2.5, outer_height/3.5]) rotate([90,0,90]) poly_penguin(carv_depth,1);
        translate([outer_len - carv_depth, outer_len/2.5, outer_height/3.5]) rotate([90,0,90]) poly_penguin(1, carv_depth);

        // Number etching
        translate([ outer_len/2 , outer_wid - carv_depth, (outer_height-top_hole_height)/2]) rotate([90,0,180]) poly_number(carv_depth, 4);
        }
    }



