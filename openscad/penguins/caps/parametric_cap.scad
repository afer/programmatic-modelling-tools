function add(v, i=0, r=0) = i<len(v) ? add(v, i+1, r+v[i]) : r;

/* body */
heights = [3,3,4]; /* Heights of the lid layers)*/
radii = [17.5,20,15];

/* holes */
nholes = 4; /* Number of holes to create */
hole_rad = 2.8; /* Radius of the hole used for tubing */
frac_dist = nholes > 1 ?  0.5 : 0; /* If nholes > 1, distance between the center and the smallest edge of the circle at which the holes are located */

/* resolution */
fangle = 2;
fsize = 0.4;

/* start of code */
hole_center_rad = min(radii) * frac_dist;

    difference()
{
    union()
    {   
        cylinder(h = heights[0], r=radii[0], $fa = fangle, $fs = fsize);
        translate([0, 0, heights[0]]) cylinder(h = heights[1], r=radii[1], $fa = fangle, $fs = fsize);
        translate([0, 0, heights[0] + heights[1]]) cylinder(h = heights[2], r = radii[2], $fa = fangle, $fs = fsize);
    }

    union()
        for (x=[0 : 360 / nholes : 360])
            {
                    /* cylinder(h = add(heights), r = hole_rad, $fa = 2, $fs = 0.4); */
                    translate ([hole_center_rad * sin(x), hole_center_rad * cos(x), 0]) cylinder(h = add(heights), r = hole_rad, $fa = 2, $fs = 0.4);
            }
}
