// Parametric model of a multi-screening microscope stage
/* use <"/home/alan/Documents/sync_docs/3D/openscad/libs/roundedcube.scad">; */
include <roundedcube.scad>;

outer_len=152;
outer_wid=102;
outer_depth=8;

slide_len = 32;
slide_wid = 22;
slide_depth=3;

hole_len = 15;
hole_wid = 10;

nslides = 5;
slides_separation = 7;

wid_across = 8;
//
slide_distance = outer_len / (slide_wid + slides_separation);
carving_len = slide_wid * nslides + slides_separation * (nslides-1);
single_carv_len = slide_wid + slides_separation;
hole_depth = outer_depth-slide_depth;

start = (outer_len-carving_len) / 2;
union(){
 difference(){
roundedcube([outer_len,outer_wid,outer_depth], radius=2);
 translate([(outer_len-carving_len)/2, wid_across, 0]) cube([carving_len, outer_wid - wid_across*2, outer_depth]);
 }
 difference(){
 translate([(outer_len-carving_len)/2, (outer_wid - slide_len - wid_across)/2, 0]) cube([carving_len, slide_len + wid_across, outer_depth]);
 union () {
   for (x=[0 : 1 : nslides-1 ]) {
     translate([ x * single_carv_len + start, (outer_wid-slide_len)/2 , hole_depth ]) cube([slide_wid, slide_len, slide_depth]);
     translate([ x * single_carv_len +  start+ (slide_wid-hole_wid)/2 , (outer_wid-hole_len)/2, 0]) cube([hole_wid, hole_len, hole_depth]);
       }
   }
 }
 }
