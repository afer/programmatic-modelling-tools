include<supports.scad>;

module distributor_body(x_bot = 400, y_bot = 20, x_top = 200, y_top = 50,
                        curvature = 5, barx = 100, bary = 10) {
  to_assert = [ x_bot, y_bot, x_top, y_top ];
  tmp =
      [for (i = to_assert) assert(2 * curvature < i, "Measurement too small")];
  x_bot = x_bot - 2 * curvature;
  x_top = x_top - 2 * curvature;
  y_bot = y_bot;
  y_top = y_top;
  difference() {
    union() {
      move(x = curvature, y = curvature) minkowski() {
        union() {
          square([ x_bot, y_bot ]);
          move(x = mid(x_bot, x_top), y = y_bot) square([ x_top, y_top ]);
        }
        circle(r = curvature);
      }
      square([ x_bot + curvature * 2, y_bot / 2 + curvature ]);
    }
    move(x = mid(x_bot, barx - 2 * bary), y = y_bot + y_top / 2 + bary / 2)
        smooth_bar(barx, bary);
  }
}
