include<utils.scad>;

module wall_row(length, n_bricks, gap) {
  brick_gap = length / n_bricks;
  lineup_x(n_bricks, brick_gap + gap) square(brick_gap - gap);
}

module lineup_walls(length, n_layers, n_bricks, gap, symmetric = true) {
  s = symmetric ? 1 : 0;
  brick_gap = length / n_bricks;
  for (i = [0:n_layers - 1]) {
    adjust = i % 2;
    translate([ adjust * brick_gap / 2, brick_gap * i, 0 ]) {
      wall_row(length + 1, n_bricks, gap);
    }
  }
}
