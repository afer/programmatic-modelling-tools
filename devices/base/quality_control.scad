include<utils.scad>;

module quality_bars(w_start = 8, w_step = -1, w_end = 1, h_start = 30,
                    h_step = -3, h_end = 9, start = 8, step = -1, end = 2) {
  spaces = [for (s = [start:step:end]) s];
  heights = [for (h = [h_start:h_step:h_end]) h];
  widths = [for (w = [w_start:w_step:w_end]) w];
  for (i = [0:len(spaces) - 1]) {
    tmp = [for (j = [0:i]) spaces[j]];
    tmp2 = [for (j = [0:i]) widths[j]];
    xs = addl(tmp) + addl(tmp2);
    move(x = xs + spaces[i]) square([ widths[i], heights[i] ], center = true);
  }
}
