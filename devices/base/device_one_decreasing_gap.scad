include<utils.scad>;
include<functions.scad>;
include<supports.scad>;
include<distributors.scad>;
include<walls.scad>;
include<traps.scad>;
include<quality_control.scad>;
include<default_parameters.scad>;

module device(dev_width = 800, dev_height = 1600, ch_width = 400,
              ch_height = 400, length = 3, width = 2, n_levels = 4,
              gaps = [ 0.5, 1.5 ], angle = 45, n_wallrows = 3, n_bricks = 50,
              wall_gap = 0.2, support_separation = 30, support_radius = 4,
              top_buffer = 120, d_yb = 50, d_yt = 20, side_buffer = 2,
              bottom_buffer = 20, traps_x_dist = 15, traps_y_dist = 10,
              tunnel_width = 50, tunnel_length = 300, inlet_rad = 100,
              text_size = 25) {

  // Convenience calculations
  // Walls
  brick_size = ch_width / n_bricks;
  wall_height = brick_size * n_wallrows - wall_gap;

  /// Traps size
  trap_width_val = basic_trap_width(width, length, angle, gaps[1]);
  trap_height_val = basic_trap_height(width, length, angle);

  // Number of traps
  max_trap_cols =
      floor((ch_width - 2 * side_buffer + trap_width_val) / traps_x_dist);
  max_trap_rows = floor(
      (ch_height - wall_height - top_buffer - bottom_buffer + trap_height_val) /
      traps_y_dist);

  trapzone_height = (max_trap_rows - 1) * traps_y_dist;
  trapzone_width = (max_trap_cols - 1) * traps_x_dist;

  gaps_list = [for (i = [gaps[0]:(gaps[1] - gaps[0]) /
                        max_trap_rows:gaps[1]]) i];
  /// Bottom support
  bot_sup_x = ch_width / 8;
  bot_sup_y = bottom_buffer / 3;

  // Assertions
  /* Check that no natural numbers are negative*/
  ns = [ max_trap_cols, max_trap_rows, traps_x_dist, traps_y_dist ];
  lst = [for (i = ns) assert(i > 0, str(i, "A naturel number is < 0"))];

  // Start display
  difference() {
    move(x = mid(dev_width, ch_width), y = mid(dev_height, ch_height)) {
      /* Chamber wall */
      move(y = ch_height - wall_height)
          lineup_walls(ch_width, n_wallrows, n_bricks, wall_gap);

      /* Traps */

      /* In the x-axis we aim for cells being in the center, in the y-axis we
       aim for the top distance to be as constant as possible. */
      move(x = mid(ch_width, trapzone_width),
           y = ch_height - wall_height - top_buffer - trapzone_height)
          decreasing_pseudomat(max_trap_cols, max_trap_rows, traps_x_dist,
                               traps_y_dist, n_levels, width, length, angle,
                               gaps_list);

      // Top support
      move(y = ch_height - wall_height) support_matrix(
          ch_width, top_buffer, support_separation, support_radius);

      // Bars that distribute media
      move(x = mid(ch_width, ch_width / 2 + bot_sup_x))
          lineup_x(2, ch_width / 2) smooth_bar(bot_sup_x, bot_sup_y);

      difference() { // Draw the entire device surrounding our shapes
        move(x = mid(ch_width, dev_width), y = mid(ch_height, dev_height))
            square([ dev_width, dev_height ]);
        union() { // Distributors and outlets
          square([ ch_width, ch_height ]);
          // Distributors
          move(y = ch_height) distributor_body(x_bot = ch_width);
          move(x = ch_width, rz = 180) distributor_body(x_bot = ch_width);
          // Straight tunnels
          move(x = mid(ch_width, tunnel_width), y = ch_height + d_yb + d_yt)
              square([ tunnel_width, tunnel_length ]);
          move(x = mid(ch_width, tunnel_width),
               y = -tunnel_length - d_yb - d_yt)
              square([ tunnel_width, tunnel_length ]);
          // inlets
          move(x = mid(ch_width, inlet_rad) + inlet_rad / 2,
               y = ch_height + d_yb + d_yt + tunnel_length) circle(inlet_rad);
          move(x = mid(ch_width, inlet_rad) + inlet_rad / 2,
               y = -(d_yb + d_yt + tunnel_length)) circle(inlet_rad);
        }
      }
    }
    move(y = text_size / 2) text("Lower left corner", size = text_size);
    move(x = dev_width, y = text_size / 2)
        text("Lower right corner", size = text_size, halign = "right");
    move(x = dev_width, y = dev_height - text_size / 2)
        text("Upper right corner", size = text_size, valign = "top",
             halign = "right");
    move(y = dev_height - 30) quality_bars();
  }
  support_outside_chamber(dev_width, dev_height, support_separation,
                          support_radius, bottom_buffer, 50);
}

module support_outside_chamber(width, height, separation, radius, bottom_buffer,
                               edge_offset) {
  difference() {
    move(y = height - edge_offset)
        support_matrix(width, height - 2 * edge_offset, separation, radius);
    move(x = mid(width, ch_width), y = mid(height, ch_height) + bottom_buffer)
        square([ ch_width, ch_height + 2 * radius - bottom_buffer ]);
  }
}

device(dev_width = dev_width, dev_height = dev_height, ch_width = ch_width,
       ch_height = ch_height, length = length, width = width, gaps = gaps,
       angle = angle, n_wallrows = n_wallrows, n_bricks = n_bricks,
       wall_gap = wall_gap, support_separation = support_separation,
       support_radius = support_radius, top_buffer = top_buffer, d_yb = d_yb,
       d_yt = d_yt, side_buffer = side_buffer, bottom_buffer = bottom_buffer,
       traps_x_dist = traps_x_dist, traps_y_dist = traps_y_dist,
       tunnel_width = tunnel_width, tunnel_length = tunnel_length,
       inlet_rad = inlet_rad, text_size = text_size);
