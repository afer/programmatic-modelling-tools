function mid(a, b) = (a - b) / 2;
function hypotenuse(width, length) = sqrt(width * width + length * length);
function basic_pillar_half_xlen(width, length, angle) = (cos(angle) * width +
                                                         sin(angle) * length) /
                                                        2;
function basic_pillar_half_ylen(width, length, angle) = (cos(angle) * length +
                                                         sin(angle) * width) /
                                                        2;
function basic_trap_width(width, length, angle, gap) =
    4 * basic_pillar_half_xlen(width, length, angle) + gap;
function basic_trap_height(width, length,
                           angle) = 2 * basic_pillar_half_ylen(width, length,
                                                               angle);
