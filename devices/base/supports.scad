module support_matrix(width, height, separation, radius) {
  ncols = floor((width) / separation);
  nrows = floor(height / (separation + 1));
  move(x = mid(width, separation * (ncols - 1)), y = -separation * nrows)
      matrix(nrows, ncols, separation) circle(radius);
}

module smooth_bar(x, y) {
  hull() { lineup_x(2, x - y) circle(y / 2); }
}
