include<utils.scad>;

module basic_pillar(width, length, angle) {
  move(rz = angle) square([ width, length ], center = true);
}

module base_trap(p_width, p_length, p_angle, gap) {
  horiz_len = basic_pillar_half_xlen(p_width, p_length, p_angle);
  union() {
    translate([ horiz_len + gap, 0, 0 ])
        basic_pillar(p_width, p_length, -p_angle);
    translate([ -(horiz_len + gap), 0, 0 ])
        basic_pillar(p_width, p_length, p_angle);
  }
}

/* module j_trap(width, length1, angle1, length2, angle2, gap) { */
/*   horiz_len1 = basic_pillar_half_xlen(width, length1, angle1); */
/*   horiz_len2 = basic_pillar_half_xlen(width, length2, (angle2 - angle1)); */
/*   vert_len1 = basic_pillar_half_ylen(width, length1, angle1); */
/*   vert_len2 = basic_pillar_half_ylen(width, length2, (angle2 - angle1)); */
/*   /\* move(rz = 45) { *\/ */
/*   /\* union() { *\/ */
/*   basic_pillar(width, length1, angle1); */
/*   /\* move(x = -horiz_len1 + width * sin(angle1) / 2, *\/ */
/*   /\*      y = vert_len1 + length2 * cos(angle2) / 2) *\/ */
/*   move(x = -horiz_len1 + width * sin(angle1) - width * cos(angle2 + angle1),
 */
/*        y = vert_len1 + vert_len2 + width * cos(angle2 + angle1)) */
/*       move(rz = angle2 - angle1) square([ width, length2 ], center = true);
 */
/*   /\* basic_pillar(width, length2, angle2); *\/ */
/*   /\* } *\/ */
/*   /\* } *\/ */
/* } */
