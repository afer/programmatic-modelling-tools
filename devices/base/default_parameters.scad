// Default parameters
$fn = 20; // Resolution of curves

/* Parameters start */
//  Device
dev_width = 800;
dev_height = 1600;

//    chamber
ch_width = 400;
ch_height = 400;

// Pillar parameters
length = 6;
width = 3.5;
gap = 1;             // For monotonous trapsets OR
gaps = [ 0.5, 1.5 ]; // for variable trapsets
angle = 45;

// Walls
n_wallrows = 3;
n_bricks = 50;
wall_gap = 0.2;

// Buffer spacing
top_buffer = 120;
side_buffer = 0;
bottom_buffer = 20;

// Distribution zone
d_yb = 50;
d_yt = 20;

// Support columns
support_separation = 40;
support_radius = 5;

// Traps zone
traps_x_dist = 20;
traps_y_dist = 15;
n_levels = 4;

tunnel_length = 300;
inlet_rad = 100;
text_size = 25;

// Connections
tunnel_width = 50;
