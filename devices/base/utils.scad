module move(x = 0, y = 0, z = 0, rx = 0, ry = 0, rz = 0) {
  // Convenience function to move objects
  translate([ x, y, z ]) rotate([ rx, ry, rz ]) children();
}

module lineup_x(num, space) {
  // Replicate objects in the x-axis
  for (i = [0:num - 1])
    move(x = space * i) children();
}
module lineup_y(num, space) {
  // Replicate objects in the y-axis
  for (i = [0:num - 1])
    move(y = space * i) children();
}

module matrix(n_rows, n_cols, separation) {
  // Replicate objects in a matrix
  lineup_y(n_rows, separation) lineup_x(n_cols, separation) children();
}

module pseudomat(x_num, y_num, x_space, y_space, n_levels) {
  // Replicate an object in a matrix with increasing offsets
  for (i = [0:y_num - 1]) {
    level = (i % n_levels);
    adjust = level * x_space / n_levels;
    i_offset = level == 0 ? 0 : 1;
    move(x = adjust, y = y_space * i) {
      lineup_x(x_num, x_space - i_offset) children();
    }
  }
}

module decreasing_pseudomat(x_num, y_num, x_space, y_space, n_levels, t_width,
                            t_length, t_angle, gaps) {
  // Replicate an object in a matrix with increasing offsets
  for (i = [0:y_num - 1]) {
    level = (i % n_levels);
    adjust = level * x_space / n_levels;
    i_offset = level == 0 ? 0 : 1;
    move(x = adjust, y = y_space * i) {
      lineup_x(x_num - i_offset, x_space)
          base_trap(width, length, angle, gaps[i]);
    }
  }
}
// Add values in a list
function addl(list, c = 0) = c < len(list) - 1 ? list[c] + addl(list, c + 1)
                                               : list[c];
